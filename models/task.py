class Task:
    """
    Abstract data model providing a unified way to represent
    tasks fetched from various providers
    """

    def __init__(self, id, title, link, is_completed, created_at, column=False, priority=""):
        """
        @param: id Task identifier
        @param: title Title of the task
        @param: link URI for accessing the task
        @param: is_completed Closure status
        @param: created_at When the task was first created
        @param: column Dashboard column where it is located
        @param: priority Level of priority, usually Low to High
        """
        self.id = id
        self.title = title
        self.link = link
        self.created_at = created_at
        self.is_completed = is_completed
        self.column = column
        self.priority = priority

    def __repr__(self):
        return "<Task {} | {} | {} | {} | {} | {} | {}>".format(
            self.id,
            self.title,
            self.link,
            self.created_at,
            self.is_completed,
            self.column,
            self.priority,
        )
