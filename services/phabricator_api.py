from config import Config
from models.task import Task
from datetime import datetime
from phabricator import Phabricator


class PhabricatorApi:
    def __init__(self):
        self.client = Phabricator(
            host=Config.PHABRICATOR_URI,
            token=Config.PHABRICATOR_KEY,
            timeout=30,
        )

    def get_user_info(self):
        """
        Display the details about the current
        user as a dictionary
        """

        return self.client.user.whoami()

    def get_tasks(self, limit=5):
        """
        Arbitrarily narrow down to only one workspace
        :param limit: how many tasks to collect
        """
        project_id = Config.PHABRICATOR_DEFAULT_PROJECT
        task_list = []
        temp_task_list = []
        cursor_overflow = True
        cursor_after = None

        # Bypass 100-result limit set by the API
        while True:

            # Check whether resultset is  larger than limit
            if cursor_after is None:
                result = self.client.maniphest.search(
                    queryKey="all",
                    constraints={"projects": [project_id]},
                    limit=limit,
                    attachments={"columns": True},
                )

            else:
                result = self.client.maniphest.search(
                    queryKey="all",
                    constraints={"projects": [project_id]},
                    limit=limit,
                    after=cursor_after,
                    attachments={"columns": True},
                )

            temp_task_list = temp_task_list + result["data"]

            # Update cursor details
            cursor_after = result["cursor"]["after"]
            if cursor_after is None:
                break

        # Parse JSON into Task objects
        for task in temp_task_list:
            task_list.append(
                Task(
                    task["id"],
                    task["fields"]["name"].strip(),
                    "{}".format(Config.PHABRICATOR_URI.replace("/api/", ""))
                    + "/T"
                    + str(task["id"]),
                    (
                        type(task["fields"]["dateClosed"]) is int
                    ),
                    datetime.strftime(
                        datetime.fromtimestamp(task["fields"]["dateCreated"]),
                        "%Y-%m-%d %H:%m:%S",
                    ),
                    task["attachments"]["columns"]["boards"][project_id]["columns"][0][
                        "name"
                    ],
                    task["fields"]["priority"]["name"]
                )
            )

        return task_list

    def get_tasks_as_json(self, limit=1):
        """
        Serialize a list tasks into JSON format
        :param limit: how many tasks to collect
        """
        tasks = self.get_tasks(limit)

        tasks = [
            {
                "id": "T{}".format(task.id),
                "title": task.title,
                "link": task.link,
                "created_at": task.created_at,
                "is_completed": task.is_completed,
                "column": task.column,
                "priority": task.priority,
            }
            for task in tasks
        ]

        return tasks