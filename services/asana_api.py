import asana
import re
from models.task import Task
from config import Config
from datetime import datetime


class AsanaApi:
    """
    Wrapper class for the Asana
    library
    """

    def __init__(self, **kwargs):
        self.client = asana.Client.access_token(Config.ASANA_KEY)
        self.default_project_id = Config.ASANA_DEFAULT_PROJECT

    def get_user_info(self):
        """
        Display the details about the current
        user as a dictionary
        """

        return self.client.users.me()

    def get_tasks_as_json(self, limit=1):
        """
        Serialize a list tasks into JSON format
        :param limit: how many tasks to collect
        """

        tasks = self.get_tasks(limit)

        tasks = [
            {
                "id": task.id,
                "title": task.title,
                "link": task.link,
                "created_at": task.created_at,
                "is_completed": task.is_completed,
                "column": task.column,
                "priority": task.priority,
            }
            for task in tasks
        ]

        return tasks

    def get_tasks(self, limit=1):
        """
        Arbitrarily narrow down to only one workspace
        :param limit: how many tasks to collect
        """
        task_list = []
        select_fields = [
            "name",
            "notes",
            "completed",
            "created_at",
            "memberships.(project|section).name",
            "custom_fields",
        ]

        # Task will be collected using an Iterator
        # which requires to set a page_size limit
        self.client.options["page_size"] = limit

        while True:
            tasks = self.client.tasks.find_by_project(
                self.default_project_id, {"opt_fields": select_fields}
            )

            # Parse JSON into Task objects
            for task in tasks:
                # Extract Phabricator ID
                phab_id = self.extract_phab_id(task["name"])
                if phab_id is False:
                    phab_id = self.extract_phab_id(task["notes"])

                # Strip auto-generated tags from task name
                task["name"] = self.sanitize_task_title(task["name"])

                task_list.append(
                    Task(
                        phab_id,
                        task["name"],
                        "{}".format("https://app.asana.com/0/0/" + task["gid"]),
                        task["completed"],
                        datetime.strftime(
                            datetime.strptime(
                                task["created_at"], "%Y-%m-%dT%H:%M:%S.%fz"
                            ),
                            "%Y-%m-%d %H:%m:%S",
                        ),
                        self.extract_column(task),
                        self.extract_priority(task),
                    )
                )

            if "next_page" not in tasks:
                break

        return task_list

    def extract_phab_id(self, text):
        """
        Get Phabricator ID out of
        the task title
        :param text: String to search in
        """

        phab_id_regex = r"[\[Maniphest\] \[.*\]]?(T\d+):"
        results = re.findall(phab_id_regex, text)
        if len(results) > 0:
            return results[0]

        return False

    def sanitize_task_title(self, text):
        """
        Remove auto-generated tags from
        the title of Asana task
        :param text: String to tweak
        """

        auto_tags_regex = r"(?:\[Maniphest\] \[.*\]*.)?T\d+: "
        text = re.sub(auto_tags_regex, "", text)

        return text

    def extract_column(self, task):
        for item in task["memberships"]:
            if item["project"]["gid"] == self.default_project_id:
                return item["section"]["name"]
        return False

    def extract_priority(self, task):
        for item in task["custom_fields"]:
            try:
                if item["name"] == "Priority" and item["display_value"] is not None:
                    return item["display_value"]
            except:
                pass

        return ""