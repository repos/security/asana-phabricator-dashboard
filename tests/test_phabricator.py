from services.phabricator_api import PhabricatorApi

class TestPhabricator:
    """
    Test suite for operations related to the
    Phabricator API
    """

    def test_get_user_info(self):
        phab = PhabricatorApi()
        user = phab.get_user_info()
        print(user)

        assert "phid" in user

    def test_get_tasks(self):
        tasks = PhabricatorApi().get_tasks()
        print(tasks)

        assert type(tasks) is list

    def test_is_task_open(self):
        raw_task_from_api = {}
        raw_task_from_api["dateClosed"] = "None"
        assert type(raw_task_from_api["dateClosed"] is not int)

        raw_task_from_api["dateClosed"] = 13300009
        assert type(raw_task_from_api["dateClosed"] is int)
