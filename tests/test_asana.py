from config import Config
from services.asana_api import AsanaApi


class TestAsana:
    """
    Test suite for operations related to the
    Asana API
    """

    def test_get_user_info(self):
        """
        Testing the get_user_info() method
        """
        user = AsanaApi().get_user_info()
        print(user)
        assert type(user) is dict

    def test_get_tasks(self):
        """
        Get a list of tasks that exists under
        a specific project
        """
        tasks = AsanaApi().get_tasks(2)
        print(tasks)
        assert type(tasks) is list