import os
from decouple import config


class Config(object):
    """
    Set of configurations that will be
    used across the application
    """

    TITLE = config("TITLE")
    DESCRIPTION = config("DESCRIPTION")
    ASANA_KEY = config("ASANA_KEY")
    ASANA_DEFAULT_PROJECT = config("ASANA_DEFAULT_PROJECT")
    PHABRICATOR_KEY = config("PHABRICATOR_KEY")
    PHABRICATOR_URI = config("PHABRICATOR_URI")
    PHABRICATOR_DEFAULT_PROJECT = config("PHABRICATOR_DEFAULT_PROJECT")
    ROOT_FOLDER = os.path.dirname(os.path.abspath(__file__))
